import { initHashRouter } from "@bjornlu/svelte-router"
import App from './App.svelte';
import Creator from './routes/Creator.svelte'
import View from './routes/View.svelte'

initHashRouter([
	{
		path: "/view/:id", component: View
	},
	{
		path: "/", component: Creator
	},
])

const app = new App({
	target: document.body,
	props: {
		name: 'world'
	}
});

export default app;
