import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setCalling } from '../reducers/callingReducer'

const Calling = (props) => {
  const { name } = props
  const dispatch = useDispatch()
  const calling = useSelector(state => state.calling)

  let style = { }

  if (calling === name) {
    style = { color: "green" }
  }

  return (
    <div>
      <h2 style={style}>{name} <button onClick={() => dispatch(setCalling(name))}>select</button></h2>

      <ul>
        { props.children }
      </ul>
    </div>
  )
}

const CallingView = () => {
  const calling = useSelector(state => state.calling)

  return (
    <div>
      <h1>Calling:</h1>

      <h2>Selected: {calling}</h2>

      <Calling name="Apostle">
        <li>+2 Ego</li>
        <li>Skills: Intimitate, Proselytize, Tactful</li>
      </Calling>

      <Calling name="Arconaut">
        <li>+2 Agility</li>
        <li>Skills: Gadget Inspector, Scanvenger, Spry, Swift Reflexes, Short Blade Expertise</li>
        <li>Starts with random junk and artifacts</li>
      </Calling>

      <Calling name="Greybeard">
        <li>+3 Willpower, -1 Strength</li>
        <li>Skills: Cudgel Proficiency, Berate, Calloused</li>
        <li>+100 reputation with Bears</li>
      </Calling>

      <Calling name="Gunslinger">
        <li>+2 Agility</li>
        <li>Skills: Steady Hands (Pistol), Weak Spotter</li>
        <li>+100 reputation with Mysterious Strangers</li>
      </Calling>

      <Calling name="Marauder">
        <li>+2 Strength</li>
        <li>Skills: Axe Proficiency, Dismember, Butchery, Charge</li>
      </Calling>

      <Calling name="Pilgrim">
        <li>+2 Willpower</li>
        <li>Skills: Meditate, Fasting Way, Iron Mind, Mind's Compass</li>
      </Calling>

      <Calling name="Nomad">
        <li>+2 Toughness</li>
        <li>Skills: Weathered, Wilderness Lore: Salt Pans, Harvestry, Mind's Compass</li>
        <li>Starts with Recycling Suit</li>
        <li>+200 reputation with The Issachari Tribe</li>
      </Calling>

      <Calling name="Scholar">
        <li>+2 Intelligence</li>
        <li>Skills: Gadget Inspector, Staunch Wounds, Heal, Wilderness Lore: Random x3, Harvestry, Hurdle</li>
      </Calling>

      <Calling name="Tinker">
        <li>+2 Intelligence</li>
        <li>Skills: Gadget Inspector, Disassemble, Lay Mine/Set Bomb, Repair, Tinker I</li>
        <li>Starts with random artifacts and scrap</li>
        <li>+100 reputation with The Barathrumites</li>
      </Calling>

      <Calling name="Warden">
        <li>+2 Strength</li>
        <li>Skills: Long Blade Proficiency, Block, Shield Slam, Steady Hands (Bows and Rifles)</li>
        <li>+400 reputation with The Fellowship of Wardens</li>
      </Calling>

      <Calling name="Water Merchant">
        <li>+2 Ego</li>
        <li>Skills: Snake Oiler</li>
        <li>Starts with trade goods</li>
        <li>+200 reputation with Water Barons</li>
      </Calling>

      <Calling name="Watervine Farmer">
        <li>+2 Toughness</li>
        <li>Skills: Cooking & Gathering, Harvestry, Axe Proficiency, Wilderness Lore: Marshes</li>
        <li>Starts with access to Farmers' Markets</li>
        <li>+100 reputation with The Villagers of Joppa</li>
      </Calling>

    </div>
  )
}

export default CallingView
