
import { createStore, combineReducers } from 'redux'

import attributeReducer from './reducers/attributeReducer'
import mutationReducer from './reducers/mutationReducer'
import callingReducer from './reducers/callingReducer'

const reducer = combineReducers({
  attributes: attributeReducer,
  mutations: mutationReducer,
  calling: callingReducer
})

const store = createStore(
  reducer
)

export default store
