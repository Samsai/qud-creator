
const backend = NODE_ENV === "development" ? BACKEND_DEV : BACKEND

const createCharacter = async (character) => {
  const resp = await fetch(backend, {
    method: 'POST',
    mode: 'cors',
    body: JSON.stringify(character)
  });

  return resp.json()
}

const getCharacter = async (id) => {
  const resp = await fetch(`${backend}/${id}`)

  return resp.json()
}

export default {
  createCharacter,
  getCharacter,
}
