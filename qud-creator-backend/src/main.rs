#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate rocket_cors;

extern crate uuid;
extern crate serde;

use rocket::http::Method;
use rocket::Request;
use rocket::State;
use rocket::response::NamedFile;

use rocket_contrib::json::{Json, JsonValue};
use rocket_contrib::serve::StaticFiles;
use rocket_cors::{AllowedHeaders, AllowedOrigins, Error};

use serde::{Deserialize, Serialize};

use std::env;
use std::path::Path;
use std::sync::RwLock;
use std::sync::atomic::AtomicUsize;
use std::collections::HashMap;

#[derive(Deserialize, Serialize, Debug)]
struct Attributes {
    attributePoints: i32,
    strength: u32,
    agility: u32,
    toughness: u32,
    intelligence: u32,
    willpower: u32,
    ego: u32
}

#[derive(Deserialize, Serialize, Debug)]
struct Mutations {
    mutationPoints: i32,
    morphotype: String,
    mutations: Vec<String>
}

#[derive(Deserialize, Serialize, Debug)]
struct Character {
    name: String,
    info: String,
    genotype: String,
    calling: String,
    attributes: Attributes,
    mutations: Mutations
}

struct InMemoryDB {
    data: HashMap<usize, Character>,
    next_id: usize,
}

#[get("/qud")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/qud/<id>")]
fn get(db: State<RwLock<InMemoryDB>>, id: usize) -> Option<JsonValue> {
    println!("Trying to fetch id: {}", id);

    let read_db = db.read().unwrap();

    read_db.data.get(&id).map(|character| json!(character))
}

#[post("/qud", data = "<character>")]
fn new(db: State<RwLock<InMemoryDB>>, character: Json<Character>) -> JsonValue {
    let mut write_db = db.write().unwrap();

    println!("{:?}", character.0);

    let id = write_db.next_id;
    write_db.next_id += 1;

    write_db.data.insert(id, character.0);

    json!({
        "status" : "ok",
        "id": id
    })
}

#[catch(404)]
fn redirect_to_index(req: &Request) -> NamedFile {
    NamedFile::open(Path::new("./static/index.html")).ok().unwrap()
}

fn main() -> Result<(), rocket_cors::Error> {
    let frontend_url = env::var("FRONTEND_URL").unwrap_or("http://localhost:5000".to_string());

    println!("Using {} as frontend...", frontend_url);

    let cors = rocket_cors::CorsOptions {
        allowed_origins: AllowedOrigins::some_exact(&[frontend_url]),
        allowed_methods: vec![Method::Get, Method::Post].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::all(),
        allow_credentials: false,
        ..Default::default()
    }
    .to_cors()?;

    rocket::ignite()
        .register(catchers![redirect_to_index])
        .mount("/api", routes![index, get, new])
        .mount("/", StaticFiles::from("./static"))
        .attach(cors)
        .manage(RwLock::new(InMemoryDB { data: HashMap::new(), next_id: 1 }))
        .launch();

    return Ok(());
}
