
import React, { useState } from 'react'

const Wizard = (props) => {
  const [stage, setStage] = useState(0)
  const children = props.children

  const go = (val) => {
    setStage(stage + (val))
    window.scrollTo(0, 0)
  }

  return (
    <div>
    {React.Children.map(children, (child, i) => {
      if (i === stage) {
        return (
          <div style={{display: "inline"}}>{child}</div>
        )
      } else {
        return (
          <div style={{display: "none"}}>{child}</div>
        )
      }
    })}

    <button onClick={() => go(-1)}>Previous</button>
    <button onClick={() => go(+1)}>Next</button>

    </div>
  )
}

export default Wizard
