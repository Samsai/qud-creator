##
# Qud Creator
#
# @file
# @version 0.1

all: backend frontend
docker: frontend
	docker build -t qud-creator .
backend:
	cd qud-creator-backend && cargo build --release
frontend:
	cd qud-creator-frontend && yarn build
	rm -rf qud-creator-backend/static
	cp -r qud-creator-frontend/build qud-creator-backend/static

# end
