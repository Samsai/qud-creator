
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import characterService from '../services/character'

const Display = (props) => {
  const {id} = useParams()

  const [character, setCharacter] = useState(null)
  const [notFound, setNotFound] = useState(false)

  useEffect(() => {
    characterService
      .get(id)
      .then(character => setCharacter(character))
      .catch(error => setNotFound(true))
  }, [id])

  if (character === null && !notFound) {
    return (
      <div>
        <h2>Loading...</h2>
      </div>
    )
  } else if (notFound) {
    return (
      <div>
        <h2>Error: Character not found.</h2>
      </div>
    )
  }

  console.log(character)

  return (
    <div>
      <h2>{character.name}</h2>

      <h3>Info:</h3>
      <p>{character.info}</p>

      <h3>Calling: {character.calling}</h3>

      <h3>Attributes:</h3>
      <ul>
        <li>Strength: {character.attributes.strength}</li>
        <li>Agility: {character.attributes.agility}</li>
        <li>Toughness: {character.attributes.toughness}</li>
        <li>Intelligence: {character.attributes.intelligence}</li>
        <li>Willpower: {character.attributes.willpower}</li>
        <li>Ego: {character.attributes.ego}</li>
      </ul>

      <h3>Mutations:</h3>

      <h4>Physical mutations:</h4>
      <ul>
        { character.mutations.physicalMutations.map((mutation, i) => (<li key={i}>{mutation}</li>)) }
      </ul>

      <h4>Mental mutations:</h4>
      <ul>
        { character.mutations.mentalMutations.map((mutation, i) => (<li key={i}>{mutation}</li>)) }
      </ul>

      <h4>Physical defects:</h4>
      <ul>
        { character.mutations.physicalDefects.map((defect, i) => (<li key={i}>{defect}</li>)) }
      </ul>

      <h4>Mental defects:</h4>
      <ul>
        { character.mutations.mentalDefects.map((defect, i) => (<li key={i}>{defect}</li>)) }
      </ul>

    </div>
  )
}

export default Display
