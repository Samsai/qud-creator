
import { writable } from 'svelte/store'

export const attributePoints = writable(44)

export const strength = writable(10)
export const agility = writable(10)
export const toughness = writable(10)
export const intelligence = writable(10)
export const willpower = writable(10)
export const ego = writable(10)
