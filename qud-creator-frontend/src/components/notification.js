
import React from 'react'
import { useSelector } from 'react-redux'

const Notification = () => {
  const notification = useSelector(state => state.notification)

  if (notification !== null) {
    return (
      <div class="notification">
        {notification}
      </div>
    )
  }

  return (
    <div></div>
  )
}
