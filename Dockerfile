FROM clux/muslrust as build-stage

COPY ./qud-creator-backend /usr/src

WORKDIR /usr/src

RUN cargo clean && cargo build --release

FROM alpine

COPY --from=build-stage /usr/src /usr/src
WORKDIR /usr/src/

CMD ["./target/x86_64-unknown-linux-musl/release/qud-creator-backend"]
