
const mutations = {
  physical_mutations: [
    {
      name: "Adrenal Control",
      cost: 4,
      excludes: []
    },
    {
      name: "Beak",
      cost: 1,
      excludes: []
    },
    {
      name: "Burrowing Claws",
      cost: 3,
      excludes: []
    },
    {
      name: "Carapace",
      cost: 3,
      excludes: ["Quills"]
    },
    {
      name: "Corrosive Gas Generation",
      cost: 3,
      excludes: []
    },
    {
      name: "Double-muscled",
      cost: 3,
      excludes: []
    },
    {
      name: "Electrical Generation",
      cost: 4,
      excludes: []
    },
    {
      name: "Electromagnetic Pulse",
      cost: 2,
      excludes: []
    },
    {
      name: "Flaming Ray",
      cost: 4,
      excludes: ["Freezing Ray"]
    },
    {
      name: "Freezing Ray",
      cost: 5,
      excludes: ["Flaming Ray"]
    },
    {
      name: "Heightened Hearing",
      cost: 2,
      excludes: []
    },
    {
      name: "Heightened Quickness",
      cost: 3,
      excludes: []
    },
    {
      name: "Horns",
      cost: 3,
      excludes: ["Psionic Migraines"]
    },
    {
      name: "Metamorphosis",
      cost: 4,
      excludes: []
    },
    {
      name: "Multiple Arms",
      cost: 4,
      excludes: []
    },
    {
      name: "Multiple Legs",
      cost: 5,
      excludes: []
    },
    {
      name: "Night Vision",
      cost: 1,
      excludes: []
    },
    {
      name: "Phasing",
      cost: 4,
      excludes: []
    },
    {
      name: "Photosynthetic Skin",
      cost: 2,
      excludes: ["Albino", "Carnivorous"]
    },
    {
      name: "Quills",
      cost: 4,
      excludes: ["Carapace"]
    },
    {
      name: "Regeneration",
      cost: 4,
      excludes: []
    },
    {
      name: "Sleep Gas Generation",
      cost: 3,
      excludes: []
    },
    {
      name: "Slime Glands",
      cost: 1,
      excludes: []
    },
    {
      name: "Spinnerets",
      cost: 3,
      excludes: []
    },
    {
      name: "Stinger (Confusing Venom)",
      cost: 3,
      excludes: ["Stinger (Paralyzing Venom)", "Stinger (Poisoning Venom)"]
    },
    {
      name: "Stinger (Paralyzing Venom)",
      cost: 4,
      excludes: ["Stinger (Confusing Venom)", "Stinger (Poisoning Venom)"]
    },
    {
      name: "Stinger (Poisoning Venom)",
      cost: 4,
      excludes: ["Stinger (Confusing Venom)", "Stinger (Paralyzing Venom)"]
    },
    {
      name: "Thick Fur",
      cost: 1,
      excludes: []
    },
    {
      name: "Triple-jointed",
      cost: 3,
      excludes: []
    },
    {
      name: "Two-hearted",
      cost: 3,
      excludes: []
    },
    {
      name: "Wings",
      cost: 4,
      excludes: []
    }
  ],
  physical_defects: [
    {
      name: "Albino",
      cost: -2,
      excludes: ["Photosynthetic Skin"]
    },
    {
      name: "Amphibious",
      cost: -3,
      excludes: []
    },
    {
      name: "Brittle Bones",
      cost: -4,
      excludes: []
    },
    {
      name: "Carnivorous",
      cost: -2,
      excludes: ["Photosynthetic Skin"]
    },
    {
      name: "Cold-Blooded",
      cost: -2,
      excludes: []
    },
    {
      name: "Electromagnetic Impulse",
      cost: -2,
      excludes: []
    },
    {
      name: "Hooks for Feet",
      cost: -4,
      excludes: []
    },
    {
      name: "Irritable Genome",
      cost: -4,
      excludes: []
    },
    {
      name: "Myopic",
      cost: -3,
      excludes: []
    },
    {
      name: "Nerve Poppy",
      cost: -3,
      excludes: []
    },
    {
      name: "Spontaneous Combustion",
      cost: -3,
      excludes: []
    },
    {
      name: "Tonic Allergy",
      cost: -4,
      excludes: []
    }
  ],
  mental_mutations: [
    {
      name: "Beguiling",
      cost: 5,
      excludes: [],
    },
    {
      name: "Burgeoning",
      cost: 3,
      excludes: [],
    },
    {
      name: "Clairvoyance",
      cost: 2,
      excludes: [],
    },
    {
      name: "Confusion",
      cost: 4,
      excludes: [],
    },
    {
      name: "Cryokinesis",
      cost: 5,
      excludes: [],
    },
    {
      name: "Disintegration",
      cost: 3,
      excludes: [],
    },
    {
      name: "Domination",
      cost: 5,
      excludes: [],
    },
    {
      name: "Ego Projection",
      cost: 4,
      excludes: [],
    },
    {
      name: "Force Bubble",
      cost: 4,
      excludes: [],
    },
    {
      name: "Force Wall",
      cost: 3,
      excludes: [],
    },
    {
      name: "Kindle",
      cost: 1,
      excludes: [],
    },
    {
      name: "Light Manipulation",
      cost: 4,
      excludes: [],
    },
    {
      name: "Mass Mind",
      cost: 4,
      excludes: [],
    },
    {
      name: "Mental Mirror",
      cost: 2,
      excludes: [],
    },
    {
      name: "Precognition",
      cost: 4,
      excludes: [],
    },
    {
      name: "Psychometry",
      cost: 4,
      excludes: ["Dystechnia"],
    },
    {
      name: "Pyrokinesis",
      cost: 5,
      excludes: [],
    },
    {
      name: "Sense Psychic",
      cost: 1,
      excludes: [],
    },
    {
      name: "Space-Time Vortex",
      cost: 3,
      excludes: [],
    },
    {
      name: "Stunning Force",
      cost: 4,
      excludes: [],
    },
    {
      name: "Sunder Mind",
      cost: 4,
      excludes: [],
    },
    {
      name: "Syphon Vim",
      cost: 3,
      excludes: [],
    },
    {
      name: "Telepathy",
      cost: 1,
      excludes: [],
    },
    {
      name: "Teleport Other",
      cost: 2,
      excludes: [],
    },
    {
      name: "Teleportation",
      cost: 5,
      excludes: [],
    },
    {
      name: "Temporal Fugue",
      cost: 5,
      excludes: [],
    },
    {
      name: "Time Dilation",
      cost: 4,
      excludes: [],
    }
  ],
  mental_defects: [
    {
      name: "Amnesia",
      cost: -2,
      excludes: []
    },
    {
      name: "Blinking Tic",
      cost: -3,
      excludes: []
    },
    {
      name: "Dystechnia",
      cost: -2,
      excludes: ["Psychometry"]
    },
    {
      name: "Evil Twin",
      cost: -3,
      excludes: []
    },
    {
      name: "Narcolepsy",
      cost: -3,
      excludes: []
    },
    {
      name: "Psionic Migraines",
      cost: -4,
      excludes: ["Horns"]
    },
    {
      name: "Quantum Jitters",
      cost: -3,
      excludes: []
    },
    {
      name: "Socially Repugnant",
      cost: -2,
      excludes: []
    }
  ]
}

export default mutations
