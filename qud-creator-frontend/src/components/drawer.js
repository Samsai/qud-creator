
import React, { useState } from 'react'

const Drawer = (props) => {
  const [ hidden, setHidden ] = useState(true)

  let style = "inline"
  let button = (<button onClick={() => setHidden(true)}>Hide</button>)

  if (hidden) {
    style = "none"
    button = (<button onClick={() => setHidden(false)}>Show</button>)
  }

  return (
    <div>
      { button }

      <div style={{display: style}}>
        { props.children }
      </div>
    </div>
  )
}

export default Drawer
