
const initialState = "Apostle"

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_CALLING":
      return action.calling
    case "RESET":
      return initialState
    default:
      return state
  }
}

export const setCalling = (calling) => {
  return {
    type: "SET_CALLING",
    calling: calling
  }
}

export default reducer
