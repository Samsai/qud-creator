import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { increaseAttribute, decreaseAttribute } from '../reducers/attributeReducer'

const Attribute = (props) => {
  const dispatch = useDispatch()
 
  const attribute = props.attribute
  const current_attribute_points = useSelector(state => state.attributes[attribute.toLowerCase()])

  return (
    <tr>
      <td>{attribute}:</td> <td>{current_attribute_points}</td>
      <td><button onClick={() => dispatch(increaseAttribute(attribute.toLowerCase()))}>+</button></td>
      <td><button onClick={() => dispatch(decreaseAttribute(attribute.toLowerCase()))}>-</button></td>
      <td></td>
    </tr>
  )
}

const Attributes = (props) => {
  const attributes = useSelector(state => state.attributes)
  const dispatch = useDispatch()
 
  return (
    <div>
      <h1>Attributes: </h1>
      <h2>Attribute points: {attributes.attributePoints}</h2>
      <hr/>

      <table>
        <Attribute attribute="Strength"/>
        <Attribute attribute="Agility"/>
        <Attribute attribute="Toughness"/>
        <Attribute attribute="Intelligence"/>
        <Attribute attribute="Willpower"/>
        <Attribute attribute="Ego"/>
      </table>
    </div>
  )
}


export default Attributes
