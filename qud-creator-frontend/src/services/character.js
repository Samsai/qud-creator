
import axios from 'axios'

let baseUrl = `${process.env.REACT_APP_BACKEND}`

if (process.env.NODE_ENV === "development") {
  baseUrl = process.env.REACT_APP_BACKEND_DEV
}

const create = async character => {
  console.log(`Uploading to: ${baseUrl}`)

  const response = await axios.post(baseUrl, character)

  return response.data
}

const get = async id => {
  console.log(`Fetching: ${baseUrl + "/" + id}`)

  const response = await axios.get(`${baseUrl}/${id}`)

  return response.data
}

export default {
  create,
  get
}
