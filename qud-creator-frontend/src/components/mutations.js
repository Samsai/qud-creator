import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  addPhysicalMutation,
  addPhysicalDefect,
  removePhysicalMutation,
  removePhysicalDefect,
  addMentalMutation,
  addMentalDefect,
  removeMentalMutation,
  removeMentalDefect,
  setMorphotype
} from '../reducers/mutationReducer'

import Drawer from './drawer'

import mutation_data from '../data/mutations'

const Mutation = (props) => {
  const { name, cost, add, remove, category } = props
  const dispatch = useDispatch()
  const mutations = useSelector(state => state.mutations)

  let added = false

  for (let [mutation_name, mutation_cost] of mutations[category]) {
    if (mutation_name === name) {
      added = true
      break
    }
  }

  let style = {}

  if (mutations.morphotype === "Esper" && category.includes("physical")) {
    style = { color: "gray" }
  }

  console.log(mutations)

  if (added) {
    return (
      <div style={style}>
        <button onClick={() => { dispatch(remove(name, cost))} }>X</button> [{cost}] {name}
      </div>
    )
  }
  return (
    <div style={style}>
      <button onClick={() => { dispatch(add(name, cost))} }>-</button> [{cost}] {name}
    </div>
  )
}

const PhysicalMutation = (props) => {
  const {name, cost} = props

  return (
    <Mutation name={name} cost={cost} add={addPhysicalMutation} remove={removePhysicalMutation} category="physicalMutations"/>
  )
}

const PhysicalDefect = (props) => {
  const {name, cost} = props

  return (
    <Mutation name={name} cost={cost} add={addPhysicalDefect} remove={removePhysicalDefect} category="physicalDefects"/>
  )
}

const MentalMutation = (props) => {
  const {name, cost} = props

  return (
    <Mutation name={name} cost={cost} add={addMentalMutation} remove={removeMentalMutation} category="mentalMutations"/>
  )
}

const MentalDefect = (props) => {
  const {name, cost} = props

  return (
    <Mutation name={name} cost={cost} add={addMentalDefect} remove={removeMentalDefect} category="mentalDefects"/>
  )
}

const Morphotype = (props) => {
  const {name} = props
  const cost = 1
  const dispatch = useDispatch()
  const morphotype = useSelector(state => state.mutations.morphotype)

  console.log(`Morphotype: ${morphotype}`)

  if (morphotype === name) {
    return (
      <div>
        <button onClick={() => { dispatch(setMorphotype(""))} }>X</button> [{cost}] {name}
      </div>
    )
  }
  return (
    <div>
      <button onClick={() => { dispatch(setMorphotype(name))} }>-</button> [{cost}] {name}
    </div>
  )
}

const Mutations = () => {
  const mutations = useSelector(state => state.mutations)

  return (
    <div>
      <h1>Mutations:</h1>

      <h2>Mutation points: {mutations.mutationPoints}</h2>

      <h2>Morphotypes</h2>

      <Drawer>
        <ul>
          <li><Morphotype name="Chimera"/></li>
          <li><Morphotype name="Esper"/></li>
          <li>WIP: Unstable Genome</li>
        </ul>
      </Drawer>

      <h2>Physical Mutations</h2>

      <Drawer>
        <ul>
          { mutation_data.physical_mutations.map((mut, index) => {
              return <li key={index}><PhysicalMutation name={mut.name} cost={mut.cost}/></li>
            })
          }
        </ul>
      </Drawer>

      <h2>Physical Defects</h2>

      <Drawer>
        <ul>

          { mutation_data.physical_defects.map((defect, index) => {
              return <li key={index}><PhysicalDefect name={defect.name} cost={defect.cost}/></li>
            })
          }
          <li><PhysicalDefect name="Albino" cost={-2}/></li>
          <li><PhysicalDefect name="Amphibious" cost={-3}/></li>
          <li><PhysicalDefect name="Analgesia" cost={-2}/></li>
          <li><PhysicalDefect name="Beak" cost={-2}/></li>
          <li><PhysicalDefect name="Cold-Blooded" cost={-2}/></li>
          <li><PhysicalDefect name="Brittle Bones" cost={-4}/></li>
          <li><PhysicalDefect name="Electromagnetic Impulse" cost={-2}/></li>
          <li><PhysicalDefect name="Hemophilia" cost={-4}/></li>
          <li><PhysicalDefect name="Hooks for Feet" cost={-4}/></li>
          <li><PhysicalDefect name="Myopia" cost={-2}/></li>
          <li><PhysicalDefect name="Spontaneous Combustion" cost={-3}/></li>
        </ul>
      </Drawer>

      <h2>Mental Mutations</h2>

      <Drawer>
        <ul>
          <li><MentalMutation name="Beguiling" cost={3}/></li>
          <li><MentalMutation name="Burgeoning" cost={3}/></li>
          <li><MentalMutation name="Clairvoyance" cost={2}/></li>
          <li><MentalMutation name="Confusion" cost={3}/></li>
          <li><MentalMutation name="Cryokinesis" cost={5}/></li>
          <li><MentalMutation name="Domination" cost={4}/></li>
          <li><MentalMutation name="Disintegration" cost={3}/></li>
          <li><MentalMutation name="Ego Projection" cost={2}/></li>
          <li><MentalMutation name="Force Bubble" cost={5}/></li>
          <li><MentalMutation name="Force Wall" cost={3}/></li>
          <li><MentalMutation name="Kindle" cost={1}/></li>
          <li><MentalMutation name="Light Manipulation" cost={4}/></li>
          <li><MentalMutation name="Mass Mind" cost={4}/></li>
          <li><MentalMutation name="Mental Mirror" cost={2}/></li>
          <li><MentalMutation name="Precognition" cost={4}/></li>
          <li><MentalMutation name="Psychometry" cost={4}/></li>
          <li><MentalMutation name="Pyrokinesis" cost={5}/></li>
          <li><MentalMutation name="Sense Psychic" cost={1}/></li>
          <li><MentalMutation name="Space-Time Vortex" cost={3}/></li>
          <li><MentalMutation name="Stunning Force" cost={4}/></li>
          <li><MentalMutation name="Sunder Mind" cost={4}/></li>
          <li><MentalMutation name="Syphon Vim" cost={3}/></li>
          <li><MentalMutation name="Telepathy" cost={1}/></li>
          <li><MentalMutation name="Teleportation" cost={5}/></li>
          <li><MentalMutation name="Teleport Other" cost={2}/></li>
          <li><MentalMutation name="Time Dilation" cost={4}/></li>
          <li><MentalMutation name="Temporal Fugue" cost={5}/></li>
        </ul>
      </Drawer>

      <h2>Mental Defects</h2>

      <Drawer>
        <ul>
          <li><MentalDefect name="Amnesia" cost={-3}/></li>
          <li><MentalDefect name="Blinking Tic" cost={-3}/></li>
          <li><MentalDefect name="Evil Twin" cost={-3}/></li>
          <li><MentalDefect name="Narcolepsy" cost={-4}/></li>
          <li><MentalDefect name="Socially Repugnant" cost={-2}/></li>
        </ul>
      </Drawer>
    </div>
  )
}

export default Mutations
