
const initialState = {
  'attributePoints': 44,
  'strength': 10,
  'agility': 10,
  'toughness': 10,
  'intelligence': 10,
  'willpower': 10,
  'ego': 10
}

const reducer = (state = initialState, action) => {
  let attribute = action.attribute;
  let current_attribute_points = state[attribute]
  let apCost = 0

  switch (action.type) {
    case 'INCREASE_ATTRIBUTE':
      if (current_attribute_points < 18) {
        apCost = 1
      } else {
        apCost = 2
      }

      if (state.attributePoints >= apCost && current_attribute_points < 24) {
        let new_state = {...state}
        new_state.attributePoints = state.attributePoints - apCost
        new_state[attribute] = current_attribute_points + 1

        return new_state
      } else {
        return state
      }
    case 'DECREASE_ATTRIBUTE':
      if (current_attribute_points <= 18) {
        apCost = 1
      } else {
        apCost = 2
      }

      if (current_attribute_points > 10) {
        let new_state = {...state}
        new_state.attributePoints = state.attributePoints + apCost
        new_state[attribute] = current_attribute_points - 1

        return new_state
      } else {
        return state
      }
    case "RESET":
      return initialState
    default:
      return state
  }
}

export const increaseAttribute = (attribute) => {
  return {
    type: 'INCREASE_ATTRIBUTE',
    attribute: attribute
  }
}

export const decreaseAttribute = (attribute) => {
  return {
    type: 'DECREASE_ATTRIBUTE',
    attribute: attribute
  }
}

export default reducer
