
const initialState = {
  mutationPoints: 12,
  morphotype: '',
  physicalMutations: [],
  physicalDefects: [],
  mentalMutations: [],
  mentalDefects: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MORPHOTYPE':
      const currentMorphotype = state.morphotype
      let cost = 0

      if (currentMorphotype === "") {
        cost = 1
      } else if (action.morphotype === "") {
        cost = -1
      }

      let newPhysicalMutations = [...state.physicalMutations]
      let newMentalMutations = [...state.mentalMutations]
      let newPhysicalDefects = [...state.physicalDefects]
      let newMentalDefects = [...state.mentalDefects]

      let points_refunded = 0

      if (action.morphotype === "Chimera") {
        points_refunded = newMentalMutations.reduce((acc, elem) => acc + elem[1], 0)
        points_refunded += newMentalDefects.reduce((acc, elem) => acc + elem[1], 0)
        newMentalMutations = []
        newMentalDefects = []
      } else if (action.morphotype === "Esper") {
        points_refunded = newPhysicalMutations.reduce((acc, elem) => acc + elem[1], 0)
        points_refunded += newPhysicalDefects.reduce((acc, elem) => acc + elem[1], 0)
        newPhysicalMutations = []
        newPhysicalDefects = []
      }

      return {
        ...state,
        mutationPoints: state.mutationPoints - cost + points_refunded,
        morphotype: action.morphotype,
        physicalMutations: newPhysicalMutations,
        mentalMutations: newMentalMutations,
        physicalDefects: newPhysicalDefects,
        mentalDefects: newMentalDefects
      }

    case 'ADD_PHYSICAL_MUTATION':
      if (state.morphotype !== "Esper" && state.mutationPoints >= action.cost) {
        return {...state,
                mutationPoints: state.mutationPoints - action.cost,
                physicalMutations: [...state.physicalMutations, [action.mutation, action.cost]].sort()}
      } else {
        return state
      }

    case "REMOVE_PHYSICAL_MUTATION":
      return {...state,
              mutationPoints: state.mutationPoints + action.cost,
              physicalMutations: state.physicalMutations
                                      .filter((element) => element !== [action.mutation, action.cost])
             }

    case 'ADD_PHYSICAL_DEFECT':
      if (state.morphotype !== "Esper" && state.physicalDefects.length + state.mentalDefects.length === 0) {
        return {...state,
                mutationPoints: state.mutationPoints - action.cost,
                physicalDefects: [...state.physicalDefects, [action.defect, action.cost]].sort()}
      } else {
        return state
      }

    case "REMOVE_PHYSICAL_DEFECT":
      if (state.physicalDefects.includes(action.defect)) {
        return {...state,
                mutationPoints: state.mutationPoints + action.cost,
                physicalDefects: state.physicalDefects
                                      .filter((element) => element !== [action.defect, action.cost])
              }
      } else {
        return state
      }

    case 'ADD_MENTAL_MUTATION':
      if (state.morphotype !== "Chimera" && state.mutationPoints >= action.cost) {
        return {...state,
                mutationPoints: state.mutationPoints - action.cost,
                mentalMutations: [...state.mentalMutations, [action.mutation, action.cost]].sort()}
      } else {
        return state
      }

    case "REMOVE_MENTAL_MUTATION":
      return {...state,
              mutationPoints: state.mutationPoints + action.cost,
              mentalMutations: state.mentalMutations
                                    .filter((element) => element !== [action.mutation, action.cost])
             }

    case 'ADD_MENTAL_DEFECT':
      if (state.morphotype !== "Chimera" && state.physicalDefects.length + state.mentalDefects.length === 0) {
        return {...state,
                mutationPoints: state.mutationPoints - action.cost,
                mentalDefects: [...state.mentalDefects, [action.defect, action.cost]].sort()}
      } else {
        return state
      }

    case "REMOVE_MENTAL_DEFECT":
      return {...state,
              mutationPoints: state.mutationPoints + action.cost,
              mentalDefects: state.mentalDefects
                                  .filter((element) => element !== [action.defect, action.cost])
             }

    case "RESET":
      return initialState

    default:
      return state
  }
}

export const setMorphotype = (morphotype) => {
  return {
    type: "SET_MORPHOTYPE",
    morphotype: morphotype
  }
}

export const addPhysicalMutation = (mutation, cost) => {
  return {
    type: "ADD_PHYSICAL_MUTATION",
    mutation: mutation,
    cost: cost
  }
}

export const removePhysicalMutation = (mutation, cost) => {
  return {
    type: "REMOVE_PHYSICAL_MUTATION",
    mutation: mutation,
    cost: cost
  }
}

export const addPhysicalDefect = (defect, cost) => {
  return {
    type: "ADD_PHYSICAL_DEFECT",
    defect: defect,
    cost: cost
  }
}

export const removePhysicalDefect = (defect, cost) => {
  return {
    type: "REMOVE_PHYSICAL_DEFECT",
    defect: defect,
    cost: cost
  }
}

export const addMentalMutation = (mutation, cost) => {
  return {
    type: "ADD_MENTAL_MUTATION",
    mutation: mutation,
    cost: cost
  }
}

export const removeMentalMutation = (mutation, cost) => {
  return {
    type: "REMOVE_MENTAL_MUTATION",
    mutation: mutation,
    cost: cost
  }
}

export const addMentalDefect = (defect, cost) => {
  return {
    type: "ADD_MENTAL_DEFECT",
    defect: defect,
    cost: cost
  }
}

export const removeMentalDefect = (defect, cost) => {
  return {
    type: "REMOVE_MENTAL_DEFECT",
    defect: defect,
    cost: cost
  }
}

export default reducer
