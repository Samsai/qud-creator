import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

import './App.css'

import Wizard from './components/wizard'
import Attributes from './components/attributes'
import Mutations from './components/mutations'
import Calling from './components/calling'
import Review from './components/review'
import Display from './components/display'

const baseUrl = process.env.PUBLIC_URL

const App = () => {
  console.log(`Backend at: ${process.env.REACT_APP_BACKEND}`)

  return (
    <div className="App">
      <header className="App-header">
        <Router basename={baseUrl}>
          <nav>
            <Link to={`/`}>Home</Link>
          </nav>

          <Switch>
            <Route path={`/character/:id`}>
              <Display/>
            </Route>

            <Route path={`/`}>
              <h1>Caves of Qud character creator</h1>

              <Wizard>
                <Attributes/>

                <Mutations/>

                <Calling/>

                <Review/>
              </Wizard>
            </Route>
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App
