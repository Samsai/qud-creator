
import { writable } from 'svelte/store'

export const mutationPoints = writable(12)
export const morphotype = writable("")
export const mutations = writable([])
export const defectSelected = writable(false)
export const exclusions = writable([])
