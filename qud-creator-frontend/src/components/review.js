import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import characterService from '../services/character'

const Review = (props) => {
  const dispatch = useDispatch()
  const [name, setName] = useState("")
  const [info, setInfo] = useState("")
  const history = useHistory()
  const attributes = useSelector(state => state.attributes)
  const mutations = useSelector(state => state.mutations)
  const calling = useSelector(state => state.calling)

  const handleName = (event) => {
    setName(event.target.value)
  }

  const handleInfo = (event) => {
    setInfo(event.target.value)
  }

  const uploadCharacter = () => {
    const character = {
      name: name,
      info: info,
      calling: calling,
      attributes: attributes,
      mutations: mutations
    }

    characterService
      .create(character)
      .then(res => {
        if (res.id !== null) {
          history.push(`/character/${res.id}`)

          dispatch({ type: "RESET" })
        }
      })
  }

  return (
    <div>
      <h2>Character overview:</h2>

      <span>
        <label>Name: </label><input onChange={handleName}></input>
      </span>

      <div>
        <p>Additional info:</p>
        <textarea rows="5" cols="50" onChange={handleInfo}>...</textarea>
      </div>

      <h3>Calling: {calling}</h3>

      <h3>Attributes:</h3>
      <ul>
        <li>Strength: {attributes.strength}</li>
        <li>Agility: {attributes.agility}</li>
        <li>Toughness: {attributes.toughness}</li>
        <li>Intelligence: {attributes.intelligence}</li>
        <li>Willpower: {attributes.willpower}</li>
        <li>Ego: {attributes.ego}</li>
      </ul>

      <h3>Mutations:</h3>

      <h4>Physical mutations:</h4>
      <ul>
        { mutations.physicalMutations.map((mutation, i) => (<li key={i}>{mutation}</li>)) }
      </ul>

      <h4>Mental mutations:</h4>
      <ul>
        { mutations.mentalMutations.map((mutation, i) => (<li key={i}>{mutation}</li>)) }
      </ul>

      <h4>Physical defects:</h4>
      <ul>
        { mutations.physicalDefects.map((defect, i) => (<li key={i}>{defect}</li>)) }
      </ul>

      <h4>Mental defects:</h4>
      <ul>
        { mutations.mentalDefects.map((defect, i) => (<li key={i}>{defect}</li>)) }
      </ul>

      <button onClick={uploadCharacter}>Upload</button>
    </div>
  )
}

export default Review
